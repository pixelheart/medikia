//
//  AppDependencyContainer.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import MedikiaiOS
import MedikiaKit

public class AppDependencyContainer{

  let sharedViewModel: MainViewModel
  let userSessionRepository: UserSessionRepository

  public init() {

    func makeUserSessionDataStore() -> UserSessionDataStore {
      return UserDefaultsSessionDataStore()
    }

    func makeUserSessionRepository() -> UserSessionRepository {
      return UserSessionRepositoryImpl(userSessionDataStore: makeUserSessionDataStore())
    }

    func makeMainViewModel() -> MainViewModel{
      return MainViewModel()
    }

    userSessionRepository = makeUserSessionRepository()
    sharedViewModel = makeMainViewModel()

  }

  public func makeMainViewController() -> MainContainerController {

    let splash = self.makeLaunchViewController()

    let authFactory = {
      return self.makeAuthViewController()
    }

    let signedInFactory = { (userSession) in
      return self.makeSignedinViewController(userSession)
    }

    return MainContainerController(
      mainViewModel: sharedViewModel,
      launchViewController: splash,
      authViewControllerFactory: authFactory,
      signedInControllerFactory: signedInFactory
    )
  }

  private func makeLaunchViewController() -> LaunchViewController {
    return LaunchViewController(launchViewModelFactory: self)
  }

  private func makeAuthViewController() -> AuthContainerController {
    return AuthDependencyContainer(appDependencyContainer: self).makeAuthViewController()
  }

  private func makeSignedinViewController(_ userSession: UserSession) -> SignedInContainerController {
    return SignedInDependencyContainer(
      userSession: userSession,
      appDependencyContainer: self
    ).makeSignedInViewController()
  }

  //MARK: - ViewModel Factory
  public func makeSplashViewModel() -> SplashViewModel {
    return SplashViewModel(
      notSignedInResponer: sharedViewModel,
      signedInResponder: sharedViewModel
    )
  }
  
  public func makeLaunchViewModel() -> LaunchViewModel {
    return LaunchViewModel(
      userSessionRepository: userSessionRepository,
      signedInResponder: sharedViewModel,
      notSignedInResponder: sharedViewModel
    )
  }

}

extension AppDependencyContainer: LaunchViewModelFactory {

}
