//
//  SignedInContainerController.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation
import UIKit
import MedikiaiOS
import MedikiaKit

public class SignedInDependencyContainer{

  let userSession: UserSession
  let userSessionRepository: UserSessionRepository
  let sharedMainViewModel: MainViewModel
  let sharedSignedInViewModel: SignedInViewModel

  public init(
    userSession: UserSession,
    appDependencyContainer: AppDependencyContainer
  ) {

    func makeSignedInViewModel() -> SignedInViewModel {
      return SignedInViewModel()
    }

    self.userSession = userSession
    self.sharedMainViewModel = appDependencyContainer.sharedViewModel
    self.userSessionRepository = appDependencyContainer.userSessionRepository
    self.sharedSignedInViewModel = makeSignedInViewModel()
  }

  public func makeSignedInViewController() -> SignedInContainerController {

    let home = self.makeHomeViewController()

    return SignedInContainerController(
      viewModel: sharedSignedInViewModel,
      homeViewController: home,
      detailViewController: UIViewController())
  }

  fileprivate func makeHomeViewController() -> HomeViewController {
    return HomeViewController(viewModelFactory: self)
  }

  public func makeHomeViewModel() -> HomeViewModel {

    return HomeViewModel(
      userSessionRepository: userSessionRepository,
      notSignedInResponder: sharedMainViewModel
    )
  }

}

extension SignedInDependencyContainer: HomeViewModelFactory {

}
