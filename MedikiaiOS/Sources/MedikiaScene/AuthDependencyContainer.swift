//
//  OnboardingDependencyContainer.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import MedikiaiOS
import MedikiaKit

public class AuthDependencyContainer{

  //From parent container
  let sharedMainViewModel: MainViewModel

  //Long-lived dependencies
  let authViewModel: AuthViewModel

  public init(appDependencyContainer: AppDependencyContainer) {
    func makeAuthViewModel() -> AuthViewModel {
      return AuthViewModel()
    }

    self.sharedMainViewModel = appDependencyContainer.sharedViewModel
    self.authViewModel = makeAuthViewModel()
  }

  public func makeAuthViewController() -> AuthContainerController {
    let login = self.makeLoginViewController()

    let registerFactory = {
      return self.makeRegisterViewController()
    }

    return AuthContainerController(
      viewModel: authViewModel,
      loginViewController: login,
      registerViewControllerFactory: registerFactory
    )
  }

  //MARK: - viewcontroller factory
  private func makeLoginViewController() -> LoginViewController {
    return LoginViewController(viewModelFactory: self)
  }

  private func makeRegisterViewController() -> RegisterViewController {
    return RegisterViewController(viewModelFactory: self)
  }

  //MARK: - viewmodel factory
  public func makeLoginViewModel() -> LoginViewModel {

    func makeUserSessionDataStore() -> UserSessionDataStore {
      return UserDefaultsSessionDataStore()
    }

    func makeRemoteDataSource() -> LoginRemoteDataSource {
      return LoginRemoteDataSourceImpl()
    }

    func makeRepository() -> LoginRepository {
      return LoginRepositoryImpl(
        userSessionDataStore: makeUserSessionDataStore(),
        remoteDataSource: makeRemoteDataSource()
      )
    }

    return LoginViewModel(
      registerNavigator: authViewModel,
      loginRepository: makeRepository(),
      signedInResponder: sharedMainViewModel
    )

  }

  public func makeRegisterViewModel() -> RegisterViewModel {
    return RegisterViewModel(loginNavigator: authViewModel)
  }

  public func makeWelcomeViewModel() -> WelcomeViewModel{
    fatalError()
  }

  public func makeAuthenticateViewModel() -> AuthenticateViewModel {
    return AuthenticateViewModel(signInResponder: sharedMainViewModel)
  }
}

extension AuthDependencyContainer: WelcomeViewModelFactory,
                                   AuthenticateViewModelFactory,
                                   LoginViewModelFactory,
                                   RegisterViewModelFactory {}
