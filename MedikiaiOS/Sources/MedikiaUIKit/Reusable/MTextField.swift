//
//  MTextField.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import SwiftUI

public struct MTextField: View {

  let label: String
  let placeholder: String
  let isSecure: Bool
  @Binding var text: String

  public init(
    label: String,
    placeholder: String,
    isSecure: Bool = false,
    text: Binding<String>
  ) {

    self.label = label
    self.placeholder = placeholder
    self.isSecure = isSecure
    self._text = text
  }

  public var body: some View {
    VStack(alignment: .leading) {
      Text(label)
        .font(
          Font.custom("Gilroy", size: 16)
            .weight(.semibold)
        )
        .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))

      if isSecure {
        SecureField(placeholder, text: $text)
          .foregroundColor(.black)
          .frame(maxWidth: .infinity, minHeight: 50)
          .padding(.horizontal, 16)
          .background(.white)
          .cornerRadius(8)
          .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 12, x: 0, y: 16)
          .textContentType(.newPassword)
      }else {
        TextField(placeholder, text: $text)
          .foregroundColor(.black)
          .frame(maxWidth: .infinity, minHeight: 50)
          .padding(.horizontal, 16)
          .background(.white)
          .cornerRadius(8)
          .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 12, x: 0, y: 16)
      }
    }
  }
}

#Preview {
  MTextField(
    label: "Password",
    placeholder: "Masukkan password Anda",
    isSecure: true,
    text: .constant("")
  )
}
