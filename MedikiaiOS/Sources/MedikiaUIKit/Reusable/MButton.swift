//
//  MButton.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import SwiftUI

public struct MButton: View {

  public let label: String
  public let tapAction: () -> Void
  @Binding public var disabled: Bool

  public init(
    label: String,
    disabled: Binding<Bool>,
    tapAction: @escaping () -> Void
  ) {
    self.label = label
    self.tapAction = tapAction
    self._disabled = disabled
  }

  public var body: some View {
    Button {
      tapAction()
    } label: {
      Text(label)
        .font(
          Font.custom("Gilroy", size: 16)
            .weight(.semibold)
        )
        .foregroundColor(.white)
        .frame(maxWidth: .infinity)

    }
    .foregroundColor(.clear)
    .frame(maxWidth: .infinity, minHeight: 48)
    .background(Color(red: 0, green: 0.13, blue: 0.38))
    .cornerRadius(8)
    .shadow(color: Color(red: 0.11, green: 0.2, blue: 0.31).opacity(0.24), radius: 12, x: 0, y: 16)
    .disabled(disabled)
  }
}


#Preview {
  MButton(
    label: "Login",
    disabled: .constant(true),
    tapAction: {}
  )
}
