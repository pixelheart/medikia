//
//  SignedInContainerController.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import MedikiaKit
import Combine

public class SignedInContainerController: NiblessNavigationController {

  let viewModel: SignedInViewModel

  //MARK: - Child View Controller
  let homeViewController: HomeViewController
  let detailViewController: UIViewController

  //MARK: - State
  private var subscriptions = Set<AnyCancellable>()

  public init(
    viewModel: SignedInViewModel,
    homeViewController: HomeViewController,
    detailViewController: UIViewController
  ) {

    self.viewModel = viewModel
    self.homeViewController = homeViewController
    self.detailViewController = detailViewController

    super.init()

  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .red
    navigationBar.isHidden = true
    self.delegate = self

    let navigationActionPublisher = viewModel.$navigationAction.eraseToAnyPublisher()
    subscribe(to: navigationActionPublisher)
  }

  func subscribe(to publisher: AnyPublisher<SignedInNavigationAction, Never>) {
    publisher
      .receive(on: DispatchQueue.main)
      .removeDuplicates()
      .sink { [weak self] action in
        guard let strongSelf = self else { return }
        strongSelf.respond(to: action)
      }.store(in: &subscriptions)
  }

  func respond(to navigationAction: SignedInNavigationAction) {
    switch navigationAction {
    case .present(let view):
      present(view: view)
    case .presented:
      break
    }
  }

  public func present(view: SignedInView) {
    switch view {
    case .home:
      presentHome()
    }
  }

  public func presentHome() {
    pushViewController(homeViewController, animated: false)
  }

  public func presentDetail() {
    pushViewController(homeViewController, animated: true)
  }

}

// MARK: - Navigation Bar Presentation
extension SignedInContainerController {

  func hideOrShowNavigationBarIfNeeded(for view: SignedInView, animated: Bool) {
    if view.hidesNavigationBar() {
      hideNavigationBar(animated: animated)
    } else {
      showNavigationBar(animated: animated)
    }
  }

  func hideNavigationBar(animated: Bool) {
    if animated {
      transitionCoordinator?.animate(alongsideTransition: { context in
        self.setNavigationBarHidden(true, animated: animated)
      })
    } else {
      setNavigationBarHidden(true, animated: false)
    }
  }

  func showNavigationBar(animated: Bool) {
    if self.isNavigationBarHidden {
      self.setNavigationBarHidden(false, animated: animated)
    }
  }
}

// MARK: - UINavigationControllerDelegate
extension SignedInContainerController: UINavigationControllerDelegate {

  public func navigationController(_ navigationController: UINavigationController,
                                   willShow viewController: UIViewController,
                                   animated: Bool) {
    guard let viewToBeShown = signedInView(associatedWith: viewController) else { return }
    hideOrShowNavigationBarIfNeeded(for: viewToBeShown, animated: animated)
  }

  public func navigationController(_ navigationController: UINavigationController,
                                   didShow viewController: UIViewController,
                                   animated: Bool) {
    guard let shownView = signedInView(associatedWith: viewController) else { return }
    viewModel.uiPresented(signedInView: shownView)
  }
}

extension SignedInContainerController {

  func signedInView(associatedWith viewController: UIViewController) -> SignedInView? {
    switch viewController {
    case is HomeViewController:
      return .home
    default:
      assertionFailure("Encountered unexpected child view controller type in OnboardingViewController")
      return nil
    }
  }

}

