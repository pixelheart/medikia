//
//  HomeViewController.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import MedikiaKit
import SwiftUI

public class HomeViewController: NiblessViewController {

  let viewModelFactory: HomeViewModelFactory
  var viewModel: HomeViewModel!

  public init(viewModelFactory: HomeViewModelFactory) {
    self.viewModelFactory = viewModelFactory
    super.init()
  }

  public override func loadView() {
    super.loadView()

    viewModel = viewModelFactory.makeHomeViewModel()
    let rootView = UIHostingController(rootView: HomeView(viewModel: viewModel))
    addFullScreen(childViewController: rootView)

  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .blue
  }

}
