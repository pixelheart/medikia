//
//  HomeView.swift
//
//
//  Created by Ilham Prabawa on 02/10/23.
//

import SwiftUI
import Combine
import MedikiaKit
import MedikiaUIKit

struct HomeView: View {

  @ObservedObject var viewModel: HomeViewModel

  @State var tabs = ["All Product",
                     "Layanan Kesehatan",
                     "Alat Kesehatan",
                     "Alat Laboratorium"]
  @State var selectedTab = "All Product"
  @State var text: String = ""

  var body: some View {
    VStack {

      HStack {
        Image(systemName: "square.grid.3x3")
          .resizable()
          .frame(width: 30, height: 30)
          .clipped()
          .padding(.leading, 20)

        Spacer()

        Button {
          viewModel.signOutConfirmation()
        } label: {
          Image(systemName: "power.circle")
            .resizable()
            .frame(width: 30, height: 30)
            .foregroundColor(.black)
            .clipped()
            .padding(.trailing, 20)
        }
      }
      .frame(maxWidth: .infinity, maxHeight: 60)
      .background(Color.white)

      ScrollView(showsIndicators: false) {

        cardView()

        secondCardView()

        thirdCardView()

        HStack {
          Image("Ellipse 362")
            .frame(width: 48, height: 48)
            .background(.white)
            .clipShape(Circle())
            .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 12, x: 0, y: 16)

          Spacer(minLength: 24)

          TextField("Search", text: .constant(""))
            .foregroundColor(.black)
            .padding(.leading, 24)
            .frame(maxWidth: .infinity, minHeight: 48)
            .background(.white)
            .cornerRadius(30)
            .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.24), radius: 12, x: 0, y: 16)

        }
        .padding(.horizontal, 16)

        horizontaList()

      }

    }
    .confirmationDialog(
      "signout",
      isPresented: $viewModel.presentDialog,
      actions: {
        Button { viewModel.signOut() } label: { Text("Ok") }
      }, message: {
        Text("Apakah Anda yakin ingin keluar dari akun?")
      }
    )

  }

  @ViewBuilder
  func cardView() -> some View {
    ZStack {
      VStack(alignment: .leading, spacing: 8) {
        HStack {
          VStack(alignment: .leading, spacing: 8) {
            Text("Solusi, Kesehatan Anda")
              .font(.system(size: 18))
              .fontWeight(.medium)

            Text("Update informasi seputar kesehatan semua bisa disini !")
              .font(.system(size: 12))
              .fontWeight(.regular)
              .lineLimit(2)
          }
          .frame(width: 180)

          Spacer()
        }
        .padding(.leading, 16)

        MButton(
          label: "Selengkapnya",
          disabled: .constant(false)
        ) {
          NLog("tap", "selengkapnya")
        }
        .frame(width: 140)
        .padding(.top, 12)
        .padding(.leading, 16)
      }
      .frame(maxWidth: .infinity)
      .padding(.vertical, 16)
      .background(
        LinearGradient(
          colors: [
            Color.white,
            Color(red: 0.98, green: 0.98, blue: 0.98),
            Color(red: 0.85, green: 0.91, blue: 0.98)
          ],
          startPoint: .topLeading,
          endPoint: .trailing
        )
      )
      .cornerRadius(16)
      .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.7), radius: 16, x: 0, y: 5)
      .padding(.horizontal, 16)

      HStack {
        Spacer()
        Image("ic_calendar", bundle: .module)
          .resizable()
          .frame(width: 200, height: 200)
          .padding(.bottom, 100)
      }

    }
  }

  @ViewBuilder
  func secondCardView() -> some View {
    ZStack {
      VStack(alignment: .leading, spacing: 8) {
        HStack {
          VStack(alignment: .leading, spacing: 8) {
            Text("Layanan Khusus")
              .font(.system(size: 18))
              .fontWeight(.medium)

            Text("Tes Covid 19, Cegah Corona Sedini Mungkin")
              .font(.system(size: 12))
              .fontWeight(.regular)
              .lineLimit(2)
          }
          .frame(width: 180)

          Spacer()
        }
        .padding(.leading, 16)

        Button {

        } label: {
          Text("Daftar Test ->")
            .font(.system(size: 18, weight: .bold))
            .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))
        }
        .padding(.top, 10)
        .padding(.leading, 23)
      }
      .frame(maxWidth: .infinity)
      .padding(.vertical, 16)
      .background(.white)
      .cornerRadius(16)
      .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 16, x: 0, y: 10)
      .padding(.horizontal, 16)

      HStack {
        Spacer()
        Image("ic_vacin", bundle: .module)
          .resizable()
          .frame(width: 130, height: 130)
          .padding(.bottom, 100)
          .padding(.trailing, 20)
      }

    }
    .frame(height: 150)
  }

  @ViewBuilder
  func thirdCardView() -> some View {
    ZStack {
      VStack(alignment: .leading, spacing: 8) {
        HStack {
          Spacer()

          VStack(alignment: .leading, spacing: 8) {
            Text("Track Pemeriksaan")
              .font(.system(size: 18))
              .fontWeight(.medium)

            Text("Kamu dapat mengecek progress pemeriksaanmu disini")
              .font(.system(size: 12))
              .fontWeight(.regular)
              .lineLimit(2)

            Button {

            } label: {
              Text("Track ->")
                .font(.system(size: 18, weight: .bold))
                .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))
            }
            .padding(.top, 8)
          }
          .frame(width: 230)

        }
        .padding(.leading, 16)


      }
      .frame(maxWidth: .infinity)
      .padding(.vertical, 16)
      .background(.white)
      .cornerRadius(16)
      .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 16, x: 0, y: 10)
      .padding(.horizontal, 16)

      HStack {
        Image("ic_magnify", bundle: .module)
          .resizable()
          .frame(width: 120, height: 100)
          .padding(.bottom, 100)
          .padding(.leading, 20)

        Spacer()
      }

    }
  }

  @ViewBuilder
  func horizontaList() -> some View {

    VStack(alignment: .leading) {

      ScrollView(.horizontal, showsIndicators: false) {

        LazyHStack {

          ForEach(tabs, id: \.self) { item in

            Text(item)
              .font(.system(size: 16, weight: .bold))
              .foregroundColor(selectedTab == item ? .white : Color.unSelectedColor)
              .onTapGesture {
                selectedTab = item
              }
              .padding(.horizontal, 16)
              .frame(minWidth: 130, maxHeight: 40)
              .background(
                selectedTab == item
                ? Color(red: 0, green: 0.13, blue: 0.38)
                : .white
              )
              .cornerRadius(30)
              .shadow(
                color: selectedTab == item
                ? Color(red: 0, green: 0.13, blue: 0.38).opacity(0.24)
                : Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.24),
                radius: 12, x: 0, y: 16
              )

          }
        }
        .padding(.horizontal, 35)
      }
      .frame(height: 80)
      .padding(.bottom, 16)

      ScrollView(.horizontal, showsIndicators: false) {
        LazyHStack {
          ForEach(0..<5) { _ in
            horizontalRowView()
          }
        }
        .padding(.horizontal, 20)
      }
    }
    .padding(.top, 25)
    .padding(.bottom, 80)
  }

  @ViewBuilder
  func horizontalRowView() -> some View {
    VStack {

      Image("ic_vacin", bundle: .module)
        .resizable()
        .aspectRatio(contentMode: .fill)
        .frame(width: 80, height: 80)
        .padding(.top, 16)
        .clipped()

      Spacer()

      HStack {
        VStack(alignment: .leading) {
          Text("Suntik Steril")
            .font(
              Font.custom("Proxima Nova", size: 14)
                .weight(.semibold)
            )
            .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))

          Text("Rp. 10.000")
            .font(
              Font.custom("Proxima Nova", size: 12)
                .weight(.semibold)
            )
            .foregroundColor(Color(red: 1, green: 0.45, blue: 0))
        }

        RoundedRectangle(cornerRadius: 4)
          .fill(Color(red: 0.7, green: 1, blue: 0.8))
          .frame(width: 60, height: 18)
          .overlay(
            Text("Ready Stok")
              .font(Font.custom("Proxima Nova", size: 10))
              .foregroundColor(Color(red: 0, green: 0.44, blue: 0.15))
          )

      }
      .padding(.vertical, 16)

    }
    .foregroundColor(.clear)
    .frame(width: 160, height: 176)
    .background(.white)
    .cornerRadius(16)
    .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16),
            radius: 12, x: 0, y: 16)
    .padding(.bottom, 16)
  }

}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView(
      viewModel: HomeViewModel(
        userSessionRepository: MockUserSessionRepository(),
        notSignedInResponder: MainViewModel()
      )
    )
  }
}
