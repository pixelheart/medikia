//
//  LaunchViewController.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import UIKit
import MedikiaKit
import Combine
import SwiftUI

public class LaunchViewController: NiblessViewController {

  private var viewModel: LaunchViewModel?
  private var subscriptions = Set<AnyCancellable>()

  public init(launchViewModelFactory: LaunchViewModelFactory) {
    self.viewModel = launchViewModelFactory.makeLaunchViewModel()
    super.init()
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    let contentView = UIHostingController(rootView: SplashRootView())
    addFullScreen(childViewController: contentView)

    viewModel?.loadUserSession()

    observeViewModel()

  }

  func observeViewModel() {
   
  }

  deinit {
    NLog("deinit", String(describing: LaunchViewController.self))
  }
}
