//
//  MainViewController.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import MedikiaKit
import Combine

public class MainContainerController: NiblessViewController {

  //MARK: - Dependency
  private let viewModel: MainViewModel

  //MARK: - ChildViewController
  let launchViewController: LaunchViewController
  var authContainerController: AuthContainerController?
  var signedInContainerController: SignedInContainerController?

  //MARK: - Factories
  let makeAuthViewController: () -> AuthContainerController
  let makeSignedInController: (UserSession) -> SignedInContainerController

  private var subscriptions = Set<AnyCancellable>()

  public init(
    mainViewModel: MainViewModel,
    launchViewController: LaunchViewController,
    authViewControllerFactory: @escaping () -> AuthContainerController,
    signedInControllerFactory: @escaping (UserSession) -> SignedInContainerController
  ) {

    self.viewModel = mainViewModel
    self.launchViewController = launchViewController
    self.makeAuthViewController = authViewControllerFactory
    self.makeSignedInController = signedInControllerFactory

    super.init()
  }

  func subscribe(to publisher: AnyPublisher<MainView, Never>) {
    publisher
      .receive(on: DispatchQueue.main)
      .sink { [weak self] view in
        guard let strongSelf = self else { return }
        strongSelf.present(view)
      }.store(in: &subscriptions)
  }

  public func present(_ view: MainView) {
    switch view {
    case .launching:
      presentLaunching()
    case .auth:
      presentAuth()
    case .signedIn(let userSession):
      presentSignedIn(userSession: userSession)
    }
  }

  public func presentLaunching() {
    addFullScreen(childViewController: launchViewController)
  }

  public func presentAuth() {
    let authViewController = makeAuthViewController()
    authViewController.modalPresentationStyle = .fullScreen
    present(authViewController, animated: true) { [weak self] in
      guard let strongSelf = self else { return }

      strongSelf.remove(childViewController: strongSelf.launchViewController)
      if let signedInViewController = strongSelf.signedInContainerController {
        strongSelf.remove(childViewController: signedInViewController)
        strongSelf.signedInContainerController = nil
      }

    }

    self.authContainerController = authViewController
  }

  public func presentSignedIn(userSession: UserSession) {
    remove(childViewController: launchViewController)

    let signedInViewControllerToPresent: SignedInContainerController
    if let vc = self.signedInContainerController {
      signedInViewControllerToPresent = vc
    } else {
      signedInViewControllerToPresent = makeSignedInController(userSession)
      self.signedInContainerController = signedInViewControllerToPresent
    }

    addFullScreen(childViewController: signedInViewControllerToPresent)

    if authContainerController?.presentingViewController != nil {
      authContainerController = nil
      dismiss(animated: true)
    }
  }

  public override func viewDidLoad() {
    super.viewDidLoad()
    observeViewModel()
  }

  private func observeViewModel() {
    let publisher = viewModel.$view.removeDuplicates().eraseToAnyPublisher()
    subscribe(to: publisher)
  }

}
