//
//  AuthContainerController.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import Foundation
import UIKit
import MedikiaKit
import Combine

public class AuthContainerController: NiblessNavigationController {

  public let viewModel: AuthViewModel

  //MARK: - Child View Controller
  let loginViewController: LoginViewController
  let makeRegisterViewController: () -> RegisterViewController

  //MARK: - State
  private var subscriptions = Set<AnyCancellable>()

  public init(
    viewModel: AuthViewModel,
    loginViewController: LoginViewController,
    registerViewControllerFactory: @escaping () -> RegisterViewController
  ) {

    self.viewModel = viewModel
    self.loginViewController = loginViewController
    self.makeRegisterViewController = registerViewControllerFactory

    super.init()

  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white
    navigationBar.isHidden = true
    self.delegate = self

    let navigationActionPublisher = viewModel.$navigationAction.eraseToAnyPublisher()
    subscribe(to: navigationActionPublisher)
  }

  func subscribe(to publisher: AnyPublisher<AuthNavigationAction, Never>) {
    publisher
      .receive(on: DispatchQueue.main)
      .removeDuplicates()
      .sink { [weak self] action in
        guard let strongSelf = self else { return }
        strongSelf.respond(to: action)
      }.store(in: &subscriptions)
  }

  func respond(to navigationAction: AuthNavigationAction) {
    switch navigationAction {
    case .present(let view):
      present(view: view)
    case .presented:
      break
    }
  }

  public func present(view: AuthView) {
    switch view {
    case .login:
      presentLogin()
    case .register:
      presentRegister()
    }
  }

  public func presentLogin() {
    if viewControllers.contains(loginViewController) {
      popViewController(animated: true)
      return
    }

    pushViewController(loginViewController, animated: true)
  }

  public func presentRegister() {
    pushViewController(makeRegisterViewController(), animated: true)
  }

}

// MARK: - Navigation Bar Presentation
extension AuthContainerController {

  func hideOrShowNavigationBarIfNeeded(for view: AuthView, animated: Bool) {
    if view.hidesNavigationBar() {
      hideNavigationBar(animated: animated)
    } else {
      showNavigationBar(animated: animated)
    }
  }

  func hideNavigationBar(animated: Bool) {
    if animated {
      transitionCoordinator?.animate(alongsideTransition: { context in
        self.setNavigationBarHidden(true, animated: animated)
      })
    } else {
      setNavigationBarHidden(true, animated: false)
    }
  }

  func showNavigationBar(animated: Bool) {
    if self.isNavigationBarHidden {
      self.setNavigationBarHidden(false, animated: animated)
    }
  }
}

// MARK: - UINavigationControllerDelegate
extension AuthContainerController: UINavigationControllerDelegate {

  public func navigationController(_ navigationController: UINavigationController,
                                   willShow viewController: UIViewController,
                                   animated: Bool) {
    guard let viewToBeShown = authView(associatedWith: viewController) else { return }
    hideOrShowNavigationBarIfNeeded(for: viewToBeShown, animated: animated)
  }

  public func navigationController(_ navigationController: UINavigationController,
                                   didShow viewController: UIViewController,
                                   animated: Bool) {
    guard let shownView = authView(associatedWith: viewController) else { return }
    viewModel.uiPresented(authView: shownView)
  }
}

extension AuthContainerController {

  func authView(associatedWith viewController: UIViewController) -> AuthView? {
    switch viewController {
    case is LoginViewController:
      return .login
    case is RegisterViewController:
      return .register
    default:
      assertionFailure("Encountered unexpected child view controller type in OnboardingViewController")
      return nil
    }
  }

}
