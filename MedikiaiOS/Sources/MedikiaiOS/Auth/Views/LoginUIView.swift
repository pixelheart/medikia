//
//  LoginUIView.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import SwiftUI
import MedikiaKit
import MedikiaUIKit

struct LoginUIView: View {

  enum Field: Hashable {
    case email
    case password
  }

  @ObservedObject var viewModel: LoginViewModel
  @FocusState private var focusField: Field?

  var body: some View {

    GeometryReader { geo in

      let proxy = geo.frame(in: .local)

      ZStack {

        ScrollView(showsIndicators: false) {

          VStack(alignment: .leading, spacing: 8) {

            Text("Hai, Selamat Datang")
              .font(
                Font.custom("Gilroy", size: 28)
                  .weight(.semibold)
              )
              .foregroundColor(Color(red: 0.11, green: 0.2, blue: 0.31))
              .padding(.top, 16)

            Text("Silahkan login untuk melanjutkan")
              .font(
                Font.custom("Proxima Nova", size: 12)
                  .weight(.semibold)
              )
              .foregroundColor(Color(red: 0.35, green: 0.45, blue: 0.58))

            Image("ic_login", bundle: .module)
              .resizable()
              .aspectRatio(contentMode: .fill)
              .frame(maxWidth: .infinity, maxHeight: 218)
              .padding(.top, 30)

            MTextField(
              label: "Email",
              placeholder: "Masukkan email Anda",
              text: $viewModel.email
            )
            .focused($focusField, equals: .email)

            HStack{
              Text("Password")
                .font(
                  Font.custom("Gilroy", size: 16)
                    .weight(.semibold)
                )
                .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))

              Spacer()

              Button {

              } label: {
                Text("Lupa Password anda ?")
                  .font(
                    Font.custom("Proxima Nova", size: 14)
                      .weight(.semibold)
                  )
                  .foregroundColor(Color(red: 0.35, green: 0.45, blue: 0.58))
              }
            }
            .padding(.top, 40)

            SecureField("Masukkan password Anda", text: $viewModel.password)
              .foregroundColor(.black)
              .frame(maxWidth: .infinity, minHeight: 50)
              .padding(.horizontal, 16)
              .background(.white)
              .cornerRadius(8)
              .shadow(color: Color(red: 0.75, green: 0.75, blue: 0.75).opacity(0.16), radius: 12, x: 0, y: 16)
              .textContentType(.newPassword)
              .focused($focusField, equals: .password)

            MButton(
              label: "Login",
              disabled: $viewModel.isLoading
            ) {
              viewModel.signIn()
            }
            .overlay(
              HStack {
                Spacer()
                Image("icon_arrow_right", bundle: .module)
                  .resizable()
                  .frame(width: 24, height: 24)
              }.padding(.trailing, 16)
            )
            .padding(.top, 40)

            HStack {
              Text("Belum punya akun ?")
                .font(Font.custom("Proxima Nova", size: 14))
                .foregroundColor(Color(red: 0.75, green: 0.75, blue: 0.75))

              Button {
                viewModel.navigateToRegister()
              } label: {
                Text("Daftar sekarang")
                  .font(
                    Font.custom("Proxima Nova", size: 14)
                      .weight(.semibold)
                  )
                  .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))
              }
            }
            .frame(maxWidth: .infinity)
            .padding(.top, 30)

            HStack {
              Image("mdi:alpha-c-circle-outline")
                .frame(width: 16, height: 16)

              Text("SILK. all right reserved.")
                .font(
                  Font.custom("Proxima Nova", size: 12)
                    .weight(.semibold)
                )
                .foregroundColor(Color(red: 0.75, green: 0.75, blue: 0.75))
            }
            .frame(maxWidth: .infinity)
            .padding(.top, 30)

          }
          .frame(maxWidth: .infinity)
          .padding(.horizontal, 16)

        }

        if !viewModel.errorMessage.isEmpty {
          Text(viewModel.errorMessage)
            .foregroundColor(.red)
            .padding(.all, 16)
            .frame(maxWidth: .infinity, minHeight: 100)
            .background(Color.pinkColor)
            .clipShape(RoundedRectangle(cornerRadius: 12))
            .padding(.horizontal, 16)
            .position(
              x: proxy.midX,
              y: geo.safeAreaInsets.top + 80
            )
        }

      }

    }

  }

}

#Preview {
  LoginUIView(
    viewModel: LoginViewModel(
      registerNavigator: AuthViewModel(),
      loginRepository: MockLoginRepository(),
      signedInResponder: MainViewModel()
    )
  )
}
