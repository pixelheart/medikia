//
//  RegisterUIView.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import Foundation

import SwiftUI
import MedikiaKit
import MedikiaUIKit

struct RegisterUIView: View {

  enum Field: Hashable {
    case firstName
    case lastName
    case nationalID
    case email
    case password
    case confirmPassword
    case phoneNumber
  }

  @ObservedObject var viewModel: RegisterViewModel
  @FocusState var focusedField: Field?

  var body: some View {

    ScrollView(showsIndicators: false) {

      VStack(alignment: .leading, spacing: 8) {

        Text("Hai, Selamat Datang")
          .font(
            Font.custom("Gilroy", size: 28)
              .weight(.semibold)
          )
          .foregroundColor(Color(red: 0.11, green: 0.2, blue: 0.31))
          .padding(.top, 16)

        Text("Silahkan daftarkan diri Anda")
          .font(
            Font.custom("Proxima Nova", size: 12)
              .weight(.semibold)
          )
          .foregroundColor(Color(red: 0.35, green: 0.45, blue: 0.58))


        Image("ic_login", bundle: .module)
          .resizable()
          .aspectRatio(contentMode: .fill)
          .frame(maxWidth: .infinity, maxHeight: 218)
          .padding(.top, 30)

        HStack {
          MTextField(
            label: "Nama Depan",
            placeholder: "John",
            text: .constant("")
          )
          .focused($focusedField, equals: .firstName)

          MTextField(
            label: "Nama Belakang",
            placeholder: "Doe",
            text: .constant("")
          )
          .focused($focusedField, equals: .lastName)
        }

        MTextField(
          label: "No. KTP",
          placeholder: "Masukkan No. KTP Anda",
          text: .constant("")
        )
        .focused($focusedField, equals: .nationalID)
        .padding(.top, 40)

        MTextField(
          label: "Email",
          placeholder: "Masukkan email Anda",
          text: .constant("")
        )
        .focused($focusedField, equals: .email)
        .padding(.top, 40)

        MTextField(
          label: "No. Telepon",
          placeholder: "Masukkan nomor telepon Anda",
          text: .constant("")
        )
        .focused($focusedField, equals: .phoneNumber)
        .padding(.top, 40)

        MTextField(
          label: "Password",
          placeholder: "Masukkan password Anda",
          isSecure: true,
          text: .constant("")
        )
        .focused($focusedField, equals: .password)
        .padding(.top, 40)

        MTextField(
          label: "Konfirmasi Password",
          placeholder: "Masukkan konfirmasi password Anda",
          isSecure: true,
          text: .constant("")
        )
        .focused($focusedField, equals: .confirmPassword)
        .padding(.top, 40)

        MButton(label: "Register", disabled: .constant(false)) {

        }
        .overlay(
          HStack {
            Spacer()
            Image("icon_arrow_right", bundle: .module)
              .resizable()
              .frame(width: 24, height: 24)
          }.padding(.trailing, 16)
        )
        .padding(.top, 40)

        HStack {
          Text("Sudah punya akun ?")
            .font(Font.custom("Proxima Nova", size: 14))
            .foregroundColor(Color(red: 0.75, green: 0.75, blue: 0.75))

          Button {
            viewModel.navigateToLogin()
          } label: {
            Text("Login sekarang")
              .font(
                Font.custom("Proxima Nova", size: 14)
                  .weight(.semibold)
              )
              .foregroundColor(Color(red: 0, green: 0.13, blue: 0.38))
          }
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 30)

        HStack {
          Image("mdi:alpha-c-circle-outline")
            .frame(width: 16, height: 16)

          Text("SILK. all right reserved.")
            .font(
              Font.custom("Proxima Nova", size: 12)
                .weight(.semibold)
            )
            .foregroundColor(Color(red: 0.75, green: 0.75, blue: 0.75))
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 40)

      }
      .frame(maxWidth: .infinity)
      .padding(.horizontal, 16)

    }

  }

}

#Preview {
  RegisterUIView(viewModel: RegisterViewModel(loginNavigator: AuthViewModel()))
}
