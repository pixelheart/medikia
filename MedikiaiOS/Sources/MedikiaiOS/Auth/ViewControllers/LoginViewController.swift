//
//  LoginViewController.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import UIKit
import MedikiaKit
import SwiftUI

public class LoginViewController: NiblessViewController {

  let viewModel: LoginViewModel

  public init(viewModelFactory: LoginViewModelFactory) {
    self.viewModel = viewModelFactory.makeLoginViewModel()

    super.init()
  }

  public override func loadView() {
    super.loadView()

    let contentView = UIHostingController(rootView: LoginUIView(viewModel: viewModel))
    addFullScreen(childViewController: contentView)
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    observeViewModel()

  }

  private func observeViewModel() {
    
  }

}
