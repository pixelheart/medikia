//
//  RegisterViewController.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import UIKit
import MedikiaKit
import SwiftUI

public class RegisterViewController: NiblessViewController {

  let viewModel: RegisterViewModel

  public init(viewModelFactory: RegisterViewModelFactory) {
    self.viewModel = viewModelFactory.makeRegisterViewModel()

    super.init()
  }

  public override func loadView() {
    super.loadView()

    let contentView = UIHostingController(rootView: RegisterUIView(viewModel: viewModel))
    addFullScreen(childViewController: contentView)
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    observeViewModel()

  }

  private func observeViewModel() {

  }

}
