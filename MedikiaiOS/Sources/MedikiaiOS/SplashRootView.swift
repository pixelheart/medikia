//
//  SplashRootView.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import SwiftUI

struct SplashRootView: View {
  var body: some View {
    Color.white
  }
}

struct SplashRootView_Previews: PreviewProvider {
  static var previews: some View {
    SplashRootView()
  }
}
