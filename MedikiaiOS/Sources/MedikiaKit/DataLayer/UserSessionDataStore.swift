//
//  UserSessionDataStore.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public protocol UserSessionDataStore {

  func readUserSession() -> AnyPublisher<UserSession?, ErrorMessage>

  func save(_ session: UserSession) -> AnyPublisher<UserSession, ErrorMessage>

  func delete(_ session: UserSession) -> AnyPublisher<UserSession, ErrorMessage>

}
