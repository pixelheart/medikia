//
//  Endpoint.swift
//  
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation

public enum Endpoint {
  public static let BASE_URL = "https://reqres.in/api"
  public static let LOGIN = BASE_URL.appending("/login")
}
