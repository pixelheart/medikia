//
//  LocalService.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public protocol LocalService {
  func parsingJSON<T>(
    of type: T.Type,
    from data: Data
  ) -> Result<T, ErrorMessage> where T: Codable

  func loadJsonFromFile(with url: String) -> Data
}

public class LocalServiceImpl: LocalService{
  
  public init(){}

  public func parsingJSON<T>(
    of type: T.Type,
    from data: Data
  ) -> Result<T, ErrorMessage> where T : Decodable, T : Encodable {
    do {
      let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]

      let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
      let model = try JSONDecoder().decode(T.self, from: jsonData)
      return .success(model)

    } catch let error {
      print("parse_error", error)
    }

    return .failure(ErrorMessage(code: -3, message: "Parse Error"))
  }

  public func loadJsonFromFile(with url: String) -> Data {
    guard let pathString = Bundle.module.url(forResource: url, withExtension: "json") else {
      fatalError("File not found")
    }

    guard let data = try? Data(contentsOf: pathString) else {
      fatalError("Unable to convert json to String")
    }

    return data
  }

}
