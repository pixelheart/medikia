//
//  ErrorMessage.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public struct ErrorMessage: Error {
  var code: Int16
  var message: String

  public init(code: Int16,
              message: String) {

    self.code = code
    self.message = message
  }
  
}
