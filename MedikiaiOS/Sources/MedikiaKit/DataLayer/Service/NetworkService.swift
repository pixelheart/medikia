//
//  NetworkService.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine
import Alamofire

public struct NetworkService {

  var subscriptions = Set<AnyCancellable>()

  public init() {}

  public func combineRequest<T: Codable>(
    of type: T.Type,
    with url: String,
    withMethod method: Method? = .get,
    withHeaders headers: [String: String] = [:],
    withParameter parameters: [String: Any] = [:],
    withEncoding encoding: Encoding? = .url
  ) -> AnyPublisher<T, ErrorMessage> {

    var httpHeaders: HTTPHeaders = [:]
    for (k, v) in headers {
      httpHeaders.add(name: k, value: v)
    }

    return self.createPublisher(
      with: url,
      withMethod: method?.getMethod(),
      withHeaders: httpHeaders,
      withParameter: parameters,
      withEncoding: encoding?.getEncoding()
    )
    .decode(type: T.self, decoder: JSONDecoder())
    .mapError { $0 as! ErrorMessage }
    .eraseToAnyPublisher()

  }

  private func createPublisher(
    with url: String,
    withMethod method: HTTPMethod? = .get,
    withHeaders headers: HTTPHeaders = [:],
    withParameter parameters: Parameters = [:],
    withEncoding encoding: ParameterEncoding? = URLEncoding.default
  ) -> AnyPublisher<Data, ErrorMessage> {

    return Future<Data, ErrorMessage> { completion in

      AF.request(
        url,
        method: method ?? .get,
        parameters: parameters,
        encoding: encoding ?? URLEncoding.default,
        headers: headers
      ) { $0.timeoutInterval = 30 }
        .responseString(
          queue: DispatchQueue.main,
          encoding: String.Encoding.utf8,
          completionHandler: { (response) in

            self.log(response)

            if let error = response.error {
              if error.responseCode == -1009 {
                completion(.failure(ErrorMessage(code: -1009, message: "Connection Problem")))
              }else if error.responseCode == -1001 {
                completion(.failure(ErrorMessage(code: -1001, message: "Connection Timeout")))
              }else {
                completion(.failure(ErrorMessage(code: -1, message: error.errorDescription!)))
              }
              return
            }

            guard let code = response.response?.statusCode else {
              completion(.failure(ErrorMessage(code: -2, message: "Can't Define Error")))
              return
            }

            if 200 ... 299 ~= code  {
              //success
              guard let data = response.data else {
                completion(.failure(ErrorMessage(code: -3, message: "Data Not Found")))
                return
              }

              completion(.success(data))
            } else if code == 400 {
              completion(.failure(.init(code: -4, message: "Maaf telah terjadi kesalahan")))
            } else if 401 ... 403 ~= code {
              completion(.failure(ErrorMessage(code: -6, message: "Unauthorized")))
            } else if code == 404 {
              guard let data = response.data else { return }
              do {
                let message = try self.parseError(data: data)
                completion(.failure(ErrorMessage(code: 404, message: message)))
              }catch {
                completion(.failure(ErrorMessage(code: -1, message: "Parse Error")))
              }
            } else if 500 ... 599 ~= code {
              //internal server error
              completion(.failure(ErrorMessage(code: -7, message: "Internal Server Error")))
            } else {
              // throw unknown error
              completion(.failure(ErrorMessage(code: -5, message: "Unkonwn Error")))
            }
          }
        )
    }.eraseToAnyPublisher()

  }

  private func parseError(data: Data) throws -> String{
    do {
      if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
        if let message = json["message"] as? String {
          return message
        }
      }
    } catch {
      throw ErrorMessage(code: -1, message: "Parse error")
    }

    return ""
  }

}

public protocol NetworkServiceFactory {
  func makeNetworkService() -> NetworkService
}

extension Method {
  func getMethod() -> HTTPMethod {
    switch self {
    case .get:
      return HTTPMethod.get
    case .post:
      return HTTPMethod.post
    case .put:
      return HTTPMethod.put
    case .delete:
      return HTTPMethod.delete
    case .patch:
      return HTTPMethod.patch
    }
  }
}

extension Encoding {
  func getEncoding() -> ParameterEncoding {
    switch self {
    case .url:
      return URLEncoding.default
    case .json:
      return JSONEncoding.default
    }
  }
}

extension NetworkService {

  public func log(_ response: AFDataResponse<String>) {
    var str = ""
    str += "URL: \(String(describing: response.request!.url)) \n"
    str += "StatusCode: \(String(describing: response.response?.statusCode)) \n"
    str += "Headers: \(String(describing: response.request!.allHTTPHeaderFields!)) \n"
    let jsonStr = String(data: response.data ?? Data(), encoding: .utf8)!
    str += "Response: \(jsonStr.replacingOccurrences(of: "\\", with: "")) \n"
    if let httpBody = response.request?.httpBody,
       let parameters = NSString(
        data: httpBody,
        encoding: String.Encoding.utf8.rawValue
       ) as? String{

      str += "Parameters: \(parameters) \n"
    }

    NLog("", str)
  }

}

public enum Method {
  case get
  case post
  case put
  case delete
  case patch
}

public enum Encoding {
  case url
  case json
}

public protocol Service {
  func request<T: Codable>(
    of type: T.Type,
    with url: String,
    withMethod method: Method?,
    withHeaders headers: [String: String],
    withParameter parameters: [String: Any],
    withEncoding encoding: Encoding?,
    completion: @escaping(Result<T, ErrorMessage>) -> Void
  )

  func combineRequest<T: Codable>(
    of type: T.Type,
    with url: String,
    withMethod method: Method?,
    withHeaders headers: [String: String],
    withParameter parameters: [String: Any],
    withEncoding encoding: Encoding?
  ) -> AnyPublisher<T, ErrorMessage>

}
