//
//  UserDefaultsSessionDataStore.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public struct UserDefaultsSessionDataStore: UserSessionDataStore {

  private let userDefaults = UserDefaults.standard

  public init() {}

  public func readUserSession() -> AnyPublisher<UserSession?, ErrorMessage> {

    return Future<UserSession?, ErrorMessage> { completion in
      if let jsonString = self.userDefaults.string(forKey: Constant.USER_SESSION),
         let jsonData = jsonString.data(using: .utf8) {

        do {
          let decoder = JSONDecoder()
          let userSession = try decoder.decode(UserSession.self, from: jsonData)
          completion(.success(userSession))
        }catch {
          completion(.failure(ErrorMessage(code: -1, message: "Failed to decode")))
        }

      }else {
        completion(.failure(ErrorMessage(code: -1, message: "Failed to read user session")))
      }

    }.eraseToAnyPublisher()
  }

  public func save(_ session: UserSession) -> AnyPublisher<UserSession, ErrorMessage> {

    return Future<UserSession, ErrorMessage> { completion in
      let encoder = JSONEncoder()
      let jsonData = try! encoder.encode(session)

      if let jsonString = String(data: jsonData, encoding: .utf8) {
        self.userDefaults.setValue(jsonString, forKey: Constant.USER_SESSION)
        completion(.success(session))
      }else {
        completion(.failure(ErrorMessage(code: -1, message: "Failed to save user session")))
      }
    }.eraseToAnyPublisher()
  }

  public func delete(_ session: UserSession) -> AnyPublisher<UserSession, ErrorMessage> {
    return Future<UserSession, ErrorMessage> { completion in
      self.userDefaults.removeObject(forKey: Constant.USER_SESSION)
      completion(.success(session))
    }.eraseToAnyPublisher()
  }


}
