//
//  MockUserSessionRepository.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public struct MockUserSessionRepository: UserSessionRepository {

  public init() {}

  public func readUserSession() -> AnyPublisher<UserSession?, ErrorMessage> {
    fatalError()
  }
  
  public func deleteUserSession() -> AnyPublisher<UserSession, ErrorMessage> {
    fatalError()
  }
  

}
