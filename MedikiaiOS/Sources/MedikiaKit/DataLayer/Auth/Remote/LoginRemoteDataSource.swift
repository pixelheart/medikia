//
//  LoginRemoteDataSource.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public protocol LoginRemoteDataSource {
  func signIn(
    with username: String,
    and password: String
  ) -> AnyPublisher<LoginResponse, ErrorMessage>
}

public struct LoginRemoteDataSourceImpl: LoginRemoteDataSource {

  private let networkManager: NetworkService

  public init() {
    networkManager = NetworkService()
  }

  public func signIn(
    with username: String,
    and password: String
  ) -> AnyPublisher<LoginResponse, ErrorMessage> {

    let headers = [
      "Accept" : "*/*",
      "Cache-Control" : "no-cache"
    ]

    return networkManager.combineRequest(
      of: LoginResponse.self,
      with: Endpoint.LOGIN,
      withMethod: .post,
      withHeaders: headers,
      withParameter: [
        "email": username,
        "password": password
      ],
      withEncoding: .json
    )
    .eraseToAnyPublisher()
  }

}
