//
//  LoginResponse.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation

public struct LoginResponse: Codable {
  public let token: String
}
