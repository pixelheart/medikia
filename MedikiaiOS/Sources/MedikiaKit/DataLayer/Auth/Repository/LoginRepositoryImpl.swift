//
//  LoginRepositoryImpl.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public struct LoginRepositoryImpl: LoginRepository {

  private let userSessionDataStore: UserSessionDataStore
  private let remoteDataSource: LoginRemoteDataSource

  public init(
    userSessionDataStore: UserSessionDataStore,
    remoteDataSource: LoginRemoteDataSource
  ) {
    self.userSessionDataStore = userSessionDataStore
    self.remoteDataSource = remoteDataSource
  }

  public func signIn(
    with username: String,
    and password: String
  ) -> AnyPublisher<UserSession, ErrorMessage> {

    remoteDataSource
      .signIn(with: username, and: password)
      .map { .init(
        name: username,
        remoteSession: .init(
          token: $0.token
        )
      )}
      .flatMap{ userSessionDataStore.save($0) }
      .eraseToAnyPublisher()

  }

}
