//
//  MockLoginRepository.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public struct MockLoginRepository: LoginRepository {

  public init() {}

  public func signIn(
    with username: String,
    and password: String
  ) -> AnyPublisher<UserSession, ErrorMessage> {
    fatalError()
  }

}
