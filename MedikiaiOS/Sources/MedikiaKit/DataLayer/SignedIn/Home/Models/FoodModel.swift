//
//  FoodModel.swift
//
//
//  Created by Ilham Prabawa on 06/11/23.
//

import Foundation

public struct FoodModel: Identifiable {
  public var id: Int
  public var  imageName: String
  public var name: String
  public var priceStr: String
}
