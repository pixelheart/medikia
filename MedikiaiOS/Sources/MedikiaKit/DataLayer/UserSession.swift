//
//  UserSession.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public class UserSession: Codable {
  
  public var name: String
  public var remoteSession: RemoteSession
  
  public init() {
    self.name = ""
    self.remoteSession = .init()
  }
  
  public init(
    name: String,
    remoteSession: RemoteSession
  ) {
    self.name = name
    self.remoteSession = remoteSession
  }
  
}

extension UserSession: Equatable {
  
  public static func == (lhs: UserSession, rhs: UserSession) -> Bool {
    return lhs.name == rhs.name
    && lhs.remoteSession == rhs.remoteSession
  }
  
}

public struct RemoteSession: Codable, Equatable {
  public let token: String
  
  public init() {
    self.token = ""
  }
  
  public init(token: String) {
    self.token = token
  }
  
  public static func == (lhs: RemoteSession, rhs: RemoteSession) -> Bool {
    return lhs.token == rhs.token
  }
  
}
