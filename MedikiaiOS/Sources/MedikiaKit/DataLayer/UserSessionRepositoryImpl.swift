//
//  UserSessionRepositoryImpl.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public struct UserSessionRepositoryImpl: UserSessionRepository {

  private let userSessionDataStore: UserSessionDataStore

  public init(userSessionDataStore: UserSessionDataStore) {
    self.userSessionDataStore = userSessionDataStore
  }

  public func readUserSession() -> AnyPublisher<UserSession?, ErrorMessage> {
    userSessionDataStore.readUserSession().eraseToAnyPublisher()
  }

  public func deleteUserSession() -> AnyPublisher<UserSession, ErrorMessage> {
    readUserSession()
      .flatMap { session in
        userSessionDataStore.delete(session!)
      }
      .eraseToAnyPublisher()
  }

}
