//
//  UserSessionRepository.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public protocol UserSessionRepository {

  func readUserSession() -> AnyPublisher<UserSession?, ErrorMessage>

  func deleteUserSession() -> AnyPublisher<UserSession, ErrorMessage>

}
