//
//  LoginRepository.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public protocol LoginRepository {
  func signIn(
    with username: String,
    and password: String
  ) -> AnyPublisher<UserSession, ErrorMessage>
}
