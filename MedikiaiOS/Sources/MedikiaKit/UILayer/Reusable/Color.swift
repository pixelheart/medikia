//
//  Color.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation
import UIKit
import SwiftUI

extension UIColor {
  public static let backgroundColor = UIColor(0xFFA451)
}

extension Color {
  public static let backgroundColor = Color(hex: 0xF3F4F9)
  public static let orangeColor = Color(hex: 0xFFA451)
  public static let creamColor = Color(hex: 0xFFFAEB)
  public static let pinkColor = Color(hex: 0xFEF0F0)
  public static let purpleColor = Color(hex: 0xF1EFF6)
  public static let unSelectedColor = Color(hex: 0x938DB5)
}

extension Color {
  public init(hex: UInt, alpha: Double = 1) {
    self.init(
      .sRGB,
      red: Double((hex >> 16) & 0xff) / 255,
      green: Double((hex >> 08) & 0xff) / 255,
      blue: Double((hex >> 00) & 0xff) / 255,
      opacity: alpha
    )
  }
}
