//
//  UITextField+Publisher.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import UIKit
import Combine

extension UITextField {

  public var textPublisher: AnyPublisher<String?, Never> {
    NotificationCenter.default.publisher(
      for: UITextField.textDidChangeNotification,
      object: self
    )
    .map { notification in
      guard let textField = notification.object as? UITextField else {
        return nil
      }

      return textField.text
    }
    .eraseToAnyPublisher()
  }

}
