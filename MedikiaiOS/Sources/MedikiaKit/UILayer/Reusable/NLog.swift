//
//  NLog.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public func NLog(_ title: String, _ message: Any) {
  print("⚡️ \(title): \(message)")
}
