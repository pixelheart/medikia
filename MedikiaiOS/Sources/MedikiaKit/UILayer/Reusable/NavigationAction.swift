//
//  NavigationAction.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public enum NavigationAction<ViewModelType>: Equatable where ViewModelType: Equatable {
  case present(view: ViewModelType)
  case presented(view: ViewModelType)
}
