//
//  RegisterViewModel.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import Foundation

public class RegisterViewModel: ObservableObject {

  private let loginNavigator: LoginNavigator

  public init(loginNavigator: LoginNavigator) {
    self.loginNavigator = loginNavigator
  }

  public func navigateToLogin() {
    loginNavigator.navigateToLogin()
  }

}

public protocol RegisterViewModelFactory {
  func makeRegisterViewModel() -> RegisterViewModel
}
