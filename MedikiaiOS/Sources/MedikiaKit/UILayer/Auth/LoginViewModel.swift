//
//  LoginViewModel.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import Foundation
import Combine

public class LoginViewModel: ObservableObject {

  private let registerNavigator: RegisterNavigator
  private let loginRepository: LoginRepository
  private let signedInResponder: SignedInResponder

  @Published public var email: String = ""
  @Published public var password: String = ""
  @Published public var errorMessage: String = ""
  @Published public var isLoading: Bool = false

  private var subscriptions = Set<AnyCancellable>()

  public init(
    registerNavigator: RegisterNavigator,
    loginRepository: LoginRepository,
    signedInResponder: SignedInResponder
  ) {
    self.registerNavigator = registerNavigator
    self.loginRepository = loginRepository
    self.signedInResponder = signedInResponder
  }

  public func navigateToRegister() {
    registerNavigator.navigateToRegister()
  }

  public func signIn() {
    isLoading = true

    loginRepository.signIn(
      with: email,
      and: password
    )
    .receive(on: RunLoop.current)
    .subscribe(on: DispatchQueue.main)
    .sink { [weak self] result in
      switch result {
      case .failure(let error):
        self?.isLoading = false
        self?.indicateError(with: error.message)
      case .finished:
        self?.isLoading = false
      }
    } receiveValue: { [weak self] userSession in
      self?.isLoading = false
      self?.signedInResponder.signedIn(to: userSession)
    }.store(in: &subscriptions)

  }

  public func indicateError(with message: String) {
    errorMessage = message
    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
      self.errorMessage = ""
    }
  }

}

public protocol LoginViewModelFactory {
  func makeLoginViewModel() -> LoginViewModel
}
