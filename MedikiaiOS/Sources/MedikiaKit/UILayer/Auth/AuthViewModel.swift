//
//  AuthViewModel.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public typealias AuthNavigationAction = NavigationAction<AuthView>

public class AuthViewModel: LoginNavigator,
                            RegisterNavigator{

  @Published public private(set) var navigationAction: AuthNavigationAction = .present(view: .login)

  public init() {}

  public func navigateToLogin() {
    navigationAction = .present(view: .login)
  }

  public func navigateToRegister() {
    navigationAction = .present(view: .register)
  }

  public func uiPresented(authView: AuthView) {
    navigationAction = .presented(view: authView)
  }

}
