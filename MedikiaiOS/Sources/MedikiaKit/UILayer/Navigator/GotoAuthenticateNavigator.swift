//
//  GotoAuthenticateNavigator.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public protocol LoginNavigator{
  func navigateToLogin()
}
