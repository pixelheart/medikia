//
//  GotoDetailNavigator.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public protocol GotoDetailNavigator {
  func navigateToDetail()
}
