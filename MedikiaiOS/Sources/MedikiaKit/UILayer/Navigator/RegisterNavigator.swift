//
//  RegisterNavigator.swift
//
//
//  Created by Ilham Prabawa on 07/11/23.
//

import Foundation

public protocol RegisterNavigator {
  func navigateToRegister()
}
