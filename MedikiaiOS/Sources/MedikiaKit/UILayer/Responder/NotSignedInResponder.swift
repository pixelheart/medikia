//
//  NotSignedInResponder.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public protocol NotSignedInResponder {
  func notSignedIn()
}
