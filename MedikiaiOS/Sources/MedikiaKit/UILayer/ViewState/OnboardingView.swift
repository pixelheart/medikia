//
//  AuthView.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public enum AuthView {
  case login
  case register

  public func hidesNavigationBar() -> Bool {
    switch self {
    case .login, .register:
      return true
    }
  }
}

extension AuthView: Equatable {
  public static func == (lhs: AuthView, rhs: AuthView) -> Bool {
    switch (lhs, rhs){
    case (.login, .login):
      return true
    case (.register, .register):
      return true
    case (.login, _),
      (.register, _):
      return false
    }
  }
}
