//
//  SignedInView.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public enum SignedInView {

  case home

  public func hidesNavigationBar() -> Bool {
    switch self {
    case .home:
      return true
    }
  }

}
