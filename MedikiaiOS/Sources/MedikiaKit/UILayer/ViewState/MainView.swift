//
//  MainView.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public enum MainView {
  case launching
  case auth
  case signedIn(userSession: UserSession)
}

extension MainView: Equatable {

  public static func == (lhs: MainView, rhs: MainView) -> Bool {
    switch (lhs, rhs){
    case (.launching, .launching):
      return true
    case (.auth, .auth):
      return true
    case let (.signedIn(l), .signedIn(r)):
      return l == r
    case (.launching, _),
      (.auth, _),
      (.signedIn, _):
      return false
    }
  }

}
