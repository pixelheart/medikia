//
//  AuthenticateViewModel.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation
import Combine

public class AuthenticateViewModel {

  private let signInResponder: SignedInResponder
  public var nameSubject = CurrentValueSubject<String, Never>("")
  public var errorMessage = PassthroughSubject<String, Never>()

  public init(signInResponder: SignedInResponder) {
    self.signInResponder = signInResponder
  }

  @objc
  public func didTapStart() {
    if nameSubject.value.isEmpty {
      errorMessage.send("Please insert your name")
      return
    }
    signInResponder.signedIn(
      to: .init(
        name: nameSubject.value,
        remoteSession: .init(
          token: ""
        )
      )
    )
  }

}

public protocol AuthenticateViewModelFactory {
  func makeAuthenticateViewModel() -> AuthenticateViewModel
}
