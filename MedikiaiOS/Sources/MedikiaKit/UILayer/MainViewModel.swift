//
//  MainViewModel.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation
import Combine

public class MainViewModel: SignedInResponder, NotSignedInResponder {

  @Published public private(set) var view: MainView = .launching

  public init(){}

  public func signedIn(to userSession: UserSession) {
    view = .signedIn(userSession: userSession)
  }

  public func notSignedIn() {
    view = .auth
  }

}
