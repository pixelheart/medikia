//
//  LaunchViewModel.swift
//
//
//  Created by Ilham Prabawa on 08/11/23.
//

import Foundation
import Combine

public class LaunchViewModel {

  let userSessionRepository: UserSessionRepository
  public let signedInResponder: SignedInResponder
  public let notSignedInResponder: NotSignedInResponder

  public var obErrorMessage = PassthroughSubject<ErrorMessage, Never>()

  private var subscriptions = Set<AnyCancellable>()

  public init(
    userSessionRepository: UserSessionRepository,
    signedInResponder: SignedInResponder,
    notSignedInResponder: NotSignedInResponder
  ) {
    self.userSessionRepository = userSessionRepository
    self.signedInResponder = signedInResponder
    self.notSignedInResponder = notSignedInResponder
  }

  public func loadUserSession() {
    userSessionRepository.readUserSession()
      .receive(on: RunLoop.current)
      .subscribe(on: DispatchQueue.main)
      .sink { [weak self] result in
        if case .failure = result {
          self?.notSignedInResponder.notSignedIn()
        }
      } receiveValue: { [weak self] userSession in
        self?.signedInResponder.signedIn(to: userSession!)
      }.store(in: &subscriptions)

  }

}

public protocol LaunchViewModelFactory {
  func makeLaunchViewModel() -> LaunchViewModel
}
