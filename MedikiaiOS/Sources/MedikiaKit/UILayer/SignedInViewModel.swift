//
//  SignedInViewModel.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public typealias SignedInNavigationAction = NavigationAction<SignedInView>

public class SignedInViewModel: GotoDetailNavigator{

  @Published public private(set) var navigationAction: SignedInNavigationAction = .present(view: .home)

  public init() {}

  public func navigateToDetail() {
    navigationAction = .present(view: .home)
  }

  public func uiPresented(signedInView: SignedInView) {
    navigationAction = .presented(view: signedInView)
  }

}
