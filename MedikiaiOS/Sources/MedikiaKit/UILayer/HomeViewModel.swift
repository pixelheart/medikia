//
//  HomeViewModel.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation
import Combine

public class HomeViewModel: ObservableObject {

  private let userSessionRepository: UserSessionRepository
  private let notSignedInResponder: NotSignedInResponder

  @Published public var presentDialog: Bool = false

  public var errorMessage = PassthroughSubject<String, Never>()

  private var subscriptions = Set<AnyCancellable>()

  public init(
    userSessionRepository: UserSessionRepository,
    notSignedInResponder: NotSignedInResponder
  ) {

    self.userSessionRepository = userSessionRepository
    self.notSignedInResponder = notSignedInResponder
  }


  public func signOutConfirmation() {
    presentDialog = true
  }

  public func signOut() {
    userSessionRepository.deleteUserSession()
      .receive(on: RunLoop.current)
      .subscribe(on: DispatchQueue.main)
      .sink { result in

      } receiveValue: { [weak self] _ in
        self?.notSignedInResponder.notSignedIn()
      }.store(in: &subscriptions)

  }

}

public protocol HomeViewModelFactory {
  func makeHomeViewModel() -> HomeViewModel
}
