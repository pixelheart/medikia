//
//  WelcomeViewModel.swift
//
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public class WelcomeViewModel {

  @objc
  public func didTapNext() {

  }

}

public protocol WelcomeViewModelFactory {
  func makeWelcomeViewModel() -> WelcomeViewModel
}
