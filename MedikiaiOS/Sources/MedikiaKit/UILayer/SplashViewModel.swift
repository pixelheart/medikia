//
//  SplashViewModel.swift
//  
//
//  Created by Ilham Prabawa on 01/10/23.
//

import Foundation

public class SplashViewModel {

  private let notSignedInResponer: NotSignedInResponder
  private let signedInResponder: SignedInResponder

  public init(notSignedInResponer: NotSignedInResponder,
       signedInResponder: SignedInResponder) {

    self.notSignedInResponer = notSignedInResponer
    self.signedInResponder = signedInResponder
  }

  public func onboarding() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
      self.notSignedInResponer.notSignedIn()
    }
  }
  
}

public protocol SplashViewModelFactory {
  func makeSplashViewModel() -> SplashViewModel
}
